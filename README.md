# vue-auction

> Application-script for internal auctions managing and betting

Presumable aplication parts:
* auction indexer job
    > gets all *auctions ids*, *form submit ids* and *time to end* then saves to remote `auctions-info.json*. 
    Clients use this list for auction's actions. List has versionining based on *date*. Checks if local auction list exist if not uses remote.
* ui
    * all auctions list
        > list of *all auctions*, *time to end*, *max bid*, am I *winning* and max *auto bid*.
    * edit / add auction
        > *edit* auction if it is clicked from list or *add* new
* saving user settings
    > saving current user *tracked auctions* and *max bid* settings in *local storage*
* jobs
    * get auction info 
        > *every 5 seconds* and *every second* last 5 mins
    * betting job


#### Technologies used:
* `webpack 2` - bundling
* `vue cli` webpack template - initialising project
* `vue js` - view library for ui (version 2)
* `vuex` - state managment (flux/redux alternative for vue)
* `vue-router` - spa routing
* `axios` - ajax requests

#### Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

Detailed explanation of template [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
```


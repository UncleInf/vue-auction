const firebase = require('firebase')
const fs = require('fs')

const config = {
    apiKey: 'AIzaSyD_Ra1eLPYmp5-dW0xe9WegLy_t1KtDbgc',
    authDomain: 'vue-auction-a9fe9.firebaseapp.com',
    databaseURL: 'https://vue-auction-a9fe9.firebaseio.com',
    projectId: 'vue-auction-a9fe9',
    storageBucket: 'vue-auction-a9fe9.appspot.com',
    messagingSenderId: '32311411223'
}

exports.update = () => {
    firebase.initializeApp(config)
    const dbVersion = firebase.database().ref('/version')

    dbVersion.once('value')
        .then(snapshot => {
            const newVersion = snapshot.val() + 1
            dbVersion.set(newVersion).then(() => {
                console.log('updated firebase version to: ' + newVersion)
                firebase.database().goOffline()
                updateFile(newVersion)
            })
        })
}

function updateFile (newVersion) {
    const filePath = 'src/utils/version.js'

    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) throw err
        const transformedData = data.toString().replace(/(const version = )(.*)/, (str, group) => group + newVersion)
        fs.writeFile(filePath, transformedData, () => console.log('update file new version to: ' + newVersion))
    })
}

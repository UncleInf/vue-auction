(function ($, loader) {
    /* global firebase */
    'use strict'

    loader.bundleUrl = 'https://vue-auction-a9fe9.firebaseapp.com/static/js/app.js'
    loader.marketId = '15035393-6807-4220-b074-53a082a465b3'
    loader.prev = notLoaded
    loader.init = notLoaded
    loader.syncAuctions = syncAuctions

    loader.commands = '' +
        'loader.init() - loads app \n' +
        'loader.prev() - loads original app \n' +
        'loader.syncAuctions() - syncs auctions to firebase'

    let $body = $('body')
    let $nav = $('aside').find('nav')
    let loggedInUser = getLoggedInUser()

    let prevAppId = 'prevApp'
    let prevAppCssUrl = '/Content/Site.css'
    let prevAppCss = ''

    if ($) {
        $(() => {
            (loader.init = init)()
            loader.prev = prev
        })
    }

    function getLoggedInUser () {
        const selector = 'div:nth-child(1) > div.k-header.nav-top > ' +
            'div.k-header.nav-top.nav-div.nav-div-right.nav-div-col > div > div:nth-child(1)'
        const finalSelector = isAppLoaded() ? `#prevApp > ${selector}` : selector
        return $(finalSelector).html().trim()
    }

    function init () {
        prepareFirstLoad(isAppLoaded()).done(() => toggleApps(true))
    }

    function isAppLoaded () {
        return !!$body.find('#app').length
    }

    function prepareFirstLoad (isAppLoaded) {
        if (isAppLoaded) {
            return getResolvedPromise()
        }

        prepareHtml()
        return downloadAppResources().done(() => {
            initialiseFireBaseApp()
            console.log(loader.commands)
        })
    }

    function getResolvedPromise () {
        return $.Deferred().resolve().promise()
    }

    function prepareHtml () {
        let $showAppButton = $('<div>')
            .text('vue-auction')
            .attr('class', 'k-button nav-button')
            .click(() => window.loader.init())

        $nav.append($showAppButton)
        $body.wrapInner($('<div>').attr('id', 'prevApp'))
        $body.append($('<div>').attr('id', 'app'))

        inlineCss()
    }

    function inlineCss () {
        $.get(prevAppCssUrl).done(data => {
            console.log(`downloaded ${prevAppCssUrl}`)
            prevAppCss = data
            $(`link[href="${prevAppCssUrl}"]`).remove()
        })
    }

    function toggleApps (showApp) {
        toggleCss(showApp)
        $body.find(`#${prevAppId}`).toggle(!showApp)
        $body.find('#app').toggle(showApp)
    }

    function toggleCss (showApp) {
        if (showApp) {
            $('#loadedInlineCss').remove()
        } else {
            $('<style/>', {
                type: 'text/css',
                id: 'loadedInlineCss',
                html: prevAppCss
            }).appendTo('head')
        }
    }

    function downloadAppResources () {
        return $.when(downloadBundle(), downloadFirebase())
            .fail(() => console.log('failed :/'))
    }

    function downloadBundle () {
        return $.getScript(loader.bundleUrl)
            .done(() => console.log('downloaded bundle.js'))
    }

    function downloadFirebase () {
        const fireBaseUrl = 'https://www.gstatic.com/firebasejs/4.1.3/firebase.js'
        return $.getScript(fireBaseUrl)
            .done(() => console.log('downloaded firebase.js'))
    }

    function initialiseFireBaseApp () {
        const config = {
            apiKey: 'AIzaSyD_Ra1eLPYmp5-dW0xe9WegLy_t1KtDbgc',
            authDomain: 'vue-auction-a9fe9.firebaseapp.com',
            databaseURL: 'https://vue-auction-a9fe9.firebaseio.com',
            projectId: 'vue-auction-a9fe9',
            storageBucket: 'vue-auction-a9fe9.appspot.com',
            messagingSenderId: '32311411223'
        }
        firebase.initializeApp(config)
    }

    function prev () {
        toggleApps(false)
    }

    function notLoaded () {
        console.log('initialise function not loaded')
    }

    function getAuctionList () {
        return $.get('/Products/GetProducts', {
            MarketId: loader.marketId
        }).fail(() => console.log(`! error fetching auction data from market ${loader.marketId}`))
    }

    function syncAuctions () {
        return new Promise(resolve => {
            getAuctionList().done(data => resolve(data))
        }).then(syncToFirebase)
    }

    function syncToFirebase (auctions) {
        let dbAuctions = firebase.database().ref(`market/${loader.marketId}`)
        return dbAuctions.set({
            synced: {
                time: new Date().getTime(),
                user: loggedInUser
            },
            auctions: auctions
        }).then(() => console.log('auctions have been synced'))
    }
}(window.jQuery, window.loader = window.loader || {}))

import Vue from 'vue'
import Router from 'vue-router'
import Auctions from '@/views/Auctions'
import MyAuctions from '@/views/MyAuctions'
import People from '@/views/People'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            redirect: '/auctions'
        },
        {
            path: '/auctions',
            name: 'auctions',
            component: Auctions
        },
        {
            path: '/my-auctions',
            name: 'myAuctions',
            component: MyAuctions
        },
        {
            path: '/people',
            name: 'people',
            component: People
        }
    ]
})

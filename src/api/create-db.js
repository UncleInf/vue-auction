import * as firebase from 'firebase'

export function createDb () {
    const config = {
        apiKey: 'AIzaSyD_Ra1eLPYmp5-dW0xe9WegLy_t1KtDbgc',
        authDomain: 'vue-auction-a9fe9.firebaseapp.com',
        databaseURL: 'https://vue-auction-a9fe9.firebaseio.com',
        projectId: 'vue-auction-a9fe9',
        storageBucket: 'vue-auction-a9fe9.appspot.com',
        messagingSenderId: '32311411223'
    }

    firebase.initializeApp(config)
    return firebase.database()
}

import { createDb } from './create-db'
import { checkLoaderExists } from '../utils'

const marketId = '15035393-6807-4220-b074-53a082a465b3'
const database = createDb()
const api = database.ref(`market/${marketId}`)

export function fetchAuctions () {
    return firebaseData(api.child('auctions').once('value'))
}

export function lastAuctionSync () {
    return firebaseData(api.child('synced').once('value'))
}

export function appLatestVersion () {
    return database.ref('/version')
}

function firebaseData (jobPromise) {
    return new Promise(resolve => {
        jobPromise.then(snapshot => resolve(snapshot.val()))
    })
}

export function syncAuctions () {
    checkLoaderExists()
    return window.loader.syncAuctions()
}

export function setMyAuctions (auctions) {
    localStorage.setItem('myAuctionsIds', auctions)
}

export function getMyAuctions () {
    return localStorage.getItem('myAuctionsIds')
}

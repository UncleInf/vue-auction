export function checkLoaderExists () {
    if (!window.loader) {
        throw new AuctionException('loader was not found')
    }
}

export function AuctionException (message) {
    return {
        time: new Date().getTime(),
        message
    }
}

import * as api from '../api'

export default {
    FETCH_AUCTIONS: ({commit}) => {
        return api.fetchAuctions().then(data => {
            commit('SET_AUCTIONS', data)
        })
    },
    FETCH_SYNCED: ({commit}) => {
        return api.lastAuctionSync().then(data => {
            commit('SET_SYNCED', data)
        })
    },
    SYNC_AUCTIONS: ({dispatch}) => {
        return api.syncAuctions().then(() => {
            dispatch('FETCH_AUCTIONS')
            dispatch('FETCH_SYNCED')
        })
    },
    LISTEN_APP_VERSION: ({commit}) => {
        return api.appLatestVersion().on('value', snapshot => {
            commit('SET_LATEST_APP_VERSION', snapshot.val())
        })
    },
    FETCH_MY_AUCTIONS: ({commit}) => {
        let myAuctionsFromLocal = api.getMyAuctions()
        myAuctionsFromLocal = myAuctionsFromLocal === null ? [] : myAuctionsFromLocal.split(',')

        commit('SET_MY_AUCTIONS', myAuctionsFromLocal)
    },
    UPDATE_MY_AUCTIONS: ({commit}, value) => {
        api.setMyAuctions(value)
        commit('SET_MY_AUCTIONS', value)
    },
    UPDATE_FILTER_TAGS_TAG: ({commit}, value) => {
        commit('PUSH_FILTER_TAGS', value)
    },
    UPDATE_FILTER_TAGS: ({commit}, value) => {
        commit('SET_FILTER_TAGS', value)
    }
}

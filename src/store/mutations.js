export default {
    SET_AUCTIONS: (state, auctions) => {
        state.auctions = auctions
    },
    SET_SYNCED: (state, synced) => {
        state.synced = synced
    },
    SET_LATEST_APP_VERSION: (state, version) => {
        state.latestAppVersion = version
    },
    SET_MY_AUCTIONS: (state, auctionsIds) => {
        state.myAuctionsIds = auctionsIds
    },
    PUSH_FILTER_TAGS: (state, tag) => {
        state.filterTags.push(tag)
    },
    SET_FILTER_TAGS: (state, tags) => {
        state.filterTags = tags
    }
}

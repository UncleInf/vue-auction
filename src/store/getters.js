import { currentAppVersion } from '../utils/version'

export default {
    isNewestVersion: state => {
        return currentAppVersion() >= state.latestAppVersion
    }
}
